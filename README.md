#  AppDev/Ops team repo provisioning 2 tier application  on AWS 

Application tiers: web, db

App team uses app_networking module provided by Network team to specify  Networking requirements

## Module usage:

```hcl
###
### App Networking setup Module provided by NetOps team to AppOps/Dev team
###

module app_network_definition {
  source = "./app_networking"                  # Module Source
  app = var.app                                # Application Name
  tiers = var.tiers                            # Application Tiers
  match_expression = var.match_expression      # Condition to assign EC2 instance to a Tier
  allow_policy = var.allow_policy              # Allowed destination Ports
  inet_access = var.inet_access                # Allow internet access to instances
}
```

Output requirement from Network Team:
```
    aws_region 
    bd_id 
    bd_name 
    cloud_zone 
    subnet 
    tenant_id 
    tenant_name 
    vpc 
    vrf_id 
    vrf_name
```

example output:

```
    aws_region = us-west-1
    bd_id = uni/tn-CISCO-it/BD-bd1
    bd_name = bd1
    cloud_zone = us-west-1a
    subnet = subnet-[10.230.231.1/24]
    tenant_id = uni/tn-CISCO-it
    tenant_name = CISCO-it
    vpc = context-[admin-vrftf]-addr-[10.230.231.1/16]
    vrf_id = uni/tn-CISCO-it/ctx-admin-vrftf
    vrf_name = admin-vrftf
```

## Referenced remote backend:

```
    data "terraform_remote_state" "networking" {
      backend = "remote"
      config = {
        organization = "cisco-dcn-ecosystem"
        workspaces = {
          name = "netops"
        }
      }
    }


```

## Remote Backend to save variables and state:
```
hostname     = "app.terraform.io"
    organization = "cisco-dcn-ecosystem"

    workspaces 
      name = "appdev"

```

## APPLICATION DETAILS

Application code is provisioned using Ansible provisioner for Terraform








