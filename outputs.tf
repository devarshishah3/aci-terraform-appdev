output "web-tier" {
  value = aws_instance.web.public_ip
}

output "db-tier" {        
  value = aws_instance.mysql.public_ip    
}

output "app-instation-url" {        
  value = "http://${aws_instance.web.public_ip}/install.php"    
}

output "app-url" {
  value = "http://${aws_instance.web.public_ip}/public/index.php"
}
