provider "aws" {
  region     = data.terraform_remote_state.networking.outputs.aws_region 
#  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

provider "aci" {
  username = var.aci_username
  password = var.aci_password
  url      = var.apic_url
  insecure = true
  version  = "0.2.1"
}
