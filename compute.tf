### Point to NetOps remote backend to get info about CIDR, subnet

data "terraform_remote_state" "networking" {
  backend = "remote"
  config = {                       
    organization = "cisco-dcn-ecosystem"
    workspaces = {                      
      name = "netops"     
    }                              
  }                                
} 

###
### App Networking setup Module provided by NetOps team to AppOps/Dev team
###

module app_network_definition {                                                                                                                
  source = "./app_networking"                  # Module Source                                                                                         
  app = var.app				       # Application Name
  tiers = var.tiers                            # Application Tiers
  match_expression = var.match_expression      # Condition to assign EC2 instance to a Tier
  allow_policy = var.allow_policy	       # Allowed destination Ports
  inet_access = var.inet_access                # Allow internet access to instances                                                                                     
}

data "aws_vpc" "capic_vpc" {
  tags = {
    Name = data.terraform_remote_state.networking.outputs.vpc 
  }
  depends_on = [module.app_network_definition]
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_image]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

data "aws_subnet" "capic_subnet" {

  filter {
    name   = "tag:Name"
    values = ["${data.terraform_remote_state.networking.outputs.subnet}"]
  }
  availability_zone = data.terraform_remote_state.networking.outputs.cloud_zone
  vpc_id            = data.aws_vpc.capic_vpc.id
  depends_on = [module.app_network_definition]
}

resource "aws_instance" "web" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  subnet_id                   = data.aws_subnet.capic_subnet.id
  associate_public_ip_address = true
  key_name                    = var.instance_key_1

  tags = {
    Name = "web"
    Role = "nginx"
  }

  provisioner "file" {
    source      = "./app_code"
    destination = "/home/ubuntu"

    connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = var.priv_key_1
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo apt-get upgrade -y",
      "sudo apt-add-repository ppa:ansible/ansible -y",
      "sudo apt-get update -y",
      "sudo apt-get install ansible -y",
      "sudo apt-get install python -y",
      "ssh-keyscan -H localhost >> ~/.ssh/known_hosts",
      "sudo ansible-playbook -i /home/ubuntu/app_code/hosts /home/ubuntu/app_code/web.yml --connection=local --extra-vars \"mysql_ip=${aws_instance.mysql.public_ip}\"",
    ]

    connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = var.priv_key_1
    }
  }

  depends_on = [aws_route.internet_route, aws_instance.mysql]
}

data "aws_route_table" "selected" {
  subnet_id = data.aws_subnet.capic_subnet.id
}

data "aws_internet_gateway" "default" {
  filter {
    name   = "attachment.vpc-id"
    values = [data.aws_vpc.capic_vpc.id]
  }
}

resource "aws_route" "internet_route" {
  route_table_id         = data.aws_route_table.selected.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = data.aws_internet_gateway.default.id
#  depends_on             = [aci_cloud_context_profile.context_profile]
}

resource "aws_instance" "mysql" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  subnet_id                   = data.aws_subnet.capic_subnet.id
  associate_public_ip_address = true
  key_name                    = var.instance_key_1

  tags = {
    Name = "db"
    Role = "mysql"
  }

  provisioner "file" {
    source      = "./app_code"
    destination = "/home/ubuntu"

    connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = var.priv_key_1
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo apt-get upgrade -y",
      "sudo apt-add-repository ppa:ansible/ansible -y",
      "sudo apt-get update -y",
      "sudo apt-get install ansible -y",
      "sudo apt-get install python -y",
      "ssh-keyscan -H localhost >> ~/.ssh/known_hosts",
      "sudo ansible-playbook -i /home/ubuntu/app_code/hosts /home/ubuntu/app_code/mysql.yml --connection=local",
    ]

    connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = var.priv_key_1
    }
  }

  depends_on = [aws_route.internet_route, module.app_network_definition]
}

