
### Point to NetOps remote backend to get info about Tenant, VRF,etc

data "terraform_remote_state" "networking" {
  backend = "remote"
  config = {
    organization = "cisco-dcn-ecosystem"
    workspaces = {
      name = "netops"
    }
  }
}

resource "aci_cloud_applicationcontainer" "app1" {
  tenant_dn = data.terraform_remote_state.networking.outputs.tenant_id
  name      = var.app
}

resource "aci_cloud_e_pg" "cloud_apic_epg" {
  count                            = length(var.tiers)
  name                             = element (var.tiers, count.index)
  cloud_applicationcontainer_dn    = aci_cloud_applicationcontainer.app1.id
  relation_fv_rs_prov              = [module.contract1.contract_name]
  relation_fv_rs_cons              = [module.contract1.contract_name]
  relation_cloud_rs_cloud_e_pg_ctx = data.terraform_remote_state.networking.outputs.vrf_name
}

resource "aci_cloud_endpoint_selector" "cloud_ep_selector" {
  count            = length(var.tiers)
  cloud_e_pg_dn    = aci_cloud_e_pg.cloud_apic_epg[count.index].id
  name             = "devnet-ep-select-${count.index}"
  match_expression = "custom:${element (var.match_expression, count.index)}"
}

resource "aci_cloud_external_e_pg" "cloud_epic_ext_epg" {
  cloud_applicationcontainer_dn    = aci_cloud_applicationcontainer.app1.id
  name                             = "admin-inet"
  relation_fv_rs_cons              = [module.contract1.contract_name]
  relation_cloud_rs_cloud_e_pg_ctx = data.terraform_remote_state.networking.outputs.vrf_name
}

resource "aci_cloud_endpoint_selectorfor_external_e_pgs" "ext_ep_selector" {
  cloud_external_e_pg_dn = aci_cloud_external_e_pg.cloud_epic_ext_epg.id
  name                   = "inet"
  subnet                 = "0.0.0.0/0"
}



module contract1 {
  source = "./contract"                                                                                                           
  tenant = data.terraform_remote_state.networking.outputs.tenant_id                                                                                         
  name = "Web"
  filter_attr = [module.filter1.filter_name, module.filter2.filter_name, module.filter3.filter_name,module.filter4.filter_name]

}

module filter1 {
  source = "./filter"
  tenant = data.terraform_remote_state.networking.outputs.tenant_id 
  name = "allow_https"
  ether_t = "ip"
  prot        = "tcp"                                                                                                           
  d_from_port = "https"                                                                                                         
  d_to_port   = "https"                                                                                                         
  stateful    = "yes"
}
module filter2 {                                  
  source = "./filter"                    
  tenant = data.terraform_remote_state.networking.outputs.tenant_id   
  name = "allow_http"                   
  ether_t = "ip"                         
  prot        = "tcp"                    
  d_from_port = "http"                           
  d_to_port   = "http"                  
  stateful    = "yes"                    
}

module filter3 {                                  
  source = "./filter"                    
  tenant = data.terraform_remote_state.networking.outputs.tenant_id   
  name = "allow_mysql"                   
  ether_t = "ip"                         
  prot        = "tcp"                    
  d_from_port = "3306"                           
  d_to_port   = "3306"                  
  stateful    = "yes"                    
}

module filter4 {                                                                                                                
  source = "./filter"                                                                                                           
  tenant = data.terraform_remote_state.networking.outputs.tenant_id                                                                                          
  name = "allow_ssh"                                                                                                          
  ether_t = "ip"                                                                                                                
  prot        = "tcp"                                                                                                           
  d_from_port = "22"                                                                                                          
  d_to_port   = "22"                                                                                                          
  stateful    = "yes"                                                                                                           
}
