variable "app" {
  type = string
  default = "app1"                                
}
variable "tiers" {
  type = list
  default = []                                
}
variable "match_expression" {        
  type = list  
  default = []
}
variable "inet_access" {
  type = string
  default = "no"
}
variable "allow_policy" {
  type = list
  default = []
}
