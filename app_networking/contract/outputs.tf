output "contract_dn" {
  description = "Contract DN"
  value       = aci_contract.this.id 
}

output "contract_name" {
  description = "Contract name"
  value       = aci_contract.this.name
}
