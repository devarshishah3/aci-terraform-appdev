resource "aci_filter" "this" {
  tenant_dn = var.tenant
  name      = var.name
}

resource "aci_filter_entry" "this" {
  name        = "${var.name}-${var.ether_t}-${var.prot}-${var.d_from_port}-${var.d_to_port}"
  filter_dn   = aci_filter.this.id
  ether_t     = var.ether_t
  prot        = var.prot
  d_from_port = var.d_from_port
  d_to_port   = var.d_to_port
  stateful    = var.stateful
}

