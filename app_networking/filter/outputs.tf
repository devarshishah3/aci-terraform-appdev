output "filter_dn" {
  description = "Filter DN"
  value       = aci_filter.this.id 
}

output "filter_name" {
  description = "Filter name"
  value       = aci_filter.this.name
}
